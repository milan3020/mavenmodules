import org.apache.log4j.Logger;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assumptions.*;


public class MyFirstTest {
    private final Calculator calculator = new Calculator();
    final static org.apache.log4j.Logger log = Logger.getLogger(MyFirstTest.class);


    @BeforeAll
    static void setup() {
        log.info("Setup went successful");
    }

    @BeforeEach
    void init() {
        log.info("@BeforeEach - executes before each test method in this class");
    }

    @DisplayName("Single test successful")
    @Test
    void testSingleSuccessTest() {
        log.info("Success(@DisplayName)");
    }

    @Test
    //@Disabled("Not implemented yet")
    void testShowSomething() {
        log.info("You won't see this message");
    }

    @AfterEach
    void tearDown() {
        log.info("@AfterEach - executed after each test method.");
    }

    @AfterAll
    static void done() {
        log.info("@AfterAll - executed after all test methods.");
    }

    @Test
    void trueAssumption() {
        System.out.println("6");
        assumeTrue(5 > 1);
        assertEquals(5 + 2, 7);
    }

    @Test
    void falseAssumption() {
        assumeFalse(5 < 1);
        assertEquals(5 + 2, 7);
    }


}
