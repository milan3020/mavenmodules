import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.IOException;
import java.sql.SQLException;

public class MainClass {

    /* Get actual class name to be printed on */
    static Logger logger = Logger.getLogger(MainClass.class);

    public static void main(String[] args)throws IOException, SQLException {
        //BasicConfigurator.configure();
        logger.debug("Hello this is a debug message");
        logger.info("Hello this is an info message");
        logger.log(Level.INFO, "some");
    }
}