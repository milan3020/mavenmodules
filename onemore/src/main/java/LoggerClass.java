public class LoggerClass {
    private String name;

    public LoggerClass(String name) {
        this.name = name;
    }

    public String greeting(String message) {
        return message + " " + name;
    }
}
